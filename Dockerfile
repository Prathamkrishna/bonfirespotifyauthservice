FROM openjdk:11-jre-slim
MAINTAINER "pratham"
COPY target/BonfireSpotifyAuthService-0.0.1-SNAPSHOT.jar SpotifyAuthService.jar
ENTRYPOINT ["java", "-jar", "/SpotifyAuthService.jar"]