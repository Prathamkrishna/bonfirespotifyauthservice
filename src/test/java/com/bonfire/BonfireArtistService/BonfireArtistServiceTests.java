//package com.example.bonfirespotifyauthservice;
//
//import SpotifyItemImageDto;
//import SpotifyUserItemsDto;
//import org.junit.jupiter.api.Test;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import static CacheMap.addArtistData;
//import static CacheMap.fetchArtistData;
//
//@SpringBootTest
//class BonfireSpotifyAuthServiceApplicationTests {
//
//    @Test
//    void contextLoads() {
//        SpotifyUserItemsDto spotifyUserItemsDto = new SpotifyUserItemsDto();
//        spotifyUserItemsDto.setExternal_urls(null);
//        spotifyUserItemsDto.setName("pratham");
//        spotifyUserItemsDto.setImages(null);
//        spotifyUserItemsDto.setGenres(null);
//
//        SpotifyUserItemsDto spotifyUserItemsDto2 = new SpotifyUserItemsDto();
//        spotifyUserItemsDto2.setExternal_urls(null);
//        spotifyUserItemsDto2.setName("geazy");
//        spotifyUserItemsDto2.setImages(null);
//        spotifyUserItemsDto2.setGenres(null);
//
//        SpotifyUserItemsDto spotifyUserItemsDto3 = new SpotifyUserItemsDto();
//        spotifyUserItemsDto3.setExternal_urls(null);
//        spotifyUserItemsDto3.setName("sopmenone");
//        spotifyUserItemsDto3.setImages(null);
//        spotifyUserItemsDto3.setGenres(null);
//
//        addArtistData("Pratham", spotifyUserItemsDto);
//        addArtistData("Geazy", spotifyUserItemsDto2);
//        addArtistData("Someone", spotifyUserItemsDto3);
//
//        System.out.println(fetchArtistData("Pratham"));
//    }
//
//}
