package com.bonfire.BonfireArtistService.dtos.fetchuseremailandprofiledtos;

import lombok.Data;

@Data
public class SpotifyUserFollowers {
    private String total;
}
