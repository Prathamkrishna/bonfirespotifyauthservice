package com.bonfire.BonfireArtistService.dtos.fetchuseremailandprofiledtos;

import com.bonfire.BonfireArtistService.dtos.FetchExternalUrlDto;
import com.bonfire.BonfireArtistService.dtos.SpotifyItemImageDto;
import lombok.Data;

@Data
public class SpotifyUserProfileItemsDto {
    private String display_name;
    private String email;
    private SpotifyUserFollowers followers;
    private FetchExternalUrlDto external_urls;
    private String id;
    private SpotifyItemImageDto[] images;
    private boolean userExists;
}
