package com.bonfire.BonfireArtistService.dtos;

import lombok.Data;

@Data
public class SpotifyItemImageDto {
    private String url;
}
