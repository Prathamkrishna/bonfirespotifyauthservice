package com.bonfire.BonfireArtistService.dtos;

import lombok.Data;

@Data
public class GetUserPlaylistsDto {
    private String description;
    private String playlistlink;
    private String playlistid;
    private String imageurl;
    private String playlistname;
    private String username;
    private String userprofilelink;
}
