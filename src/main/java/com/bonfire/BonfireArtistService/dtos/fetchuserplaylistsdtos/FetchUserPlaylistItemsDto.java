package com.bonfire.BonfireArtistService.dtos.fetchuserplaylistsdtos;

import lombok.Data;

@Data
public class FetchUserPlaylistItemsDto {
    private FetchUserPlaylistInfoDto[] items;
}
