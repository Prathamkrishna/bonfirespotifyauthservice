package com.bonfire.BonfireArtistService.dtos.fetchuserplaylistsdtos;

import lombok.Data;

@Data
public class FetchUserProfileUrlDto {
    private String spotify;
}
