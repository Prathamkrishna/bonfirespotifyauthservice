package com.bonfire.BonfireArtistService.dtos.fetchuserplaylistsdtos;

import com.bonfire.BonfireArtistService.dtos.FetchExternalUrlDto;
import com.bonfire.BonfireArtistService.dtos.SpotifyItemImageDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class FetchUserPlaylistInfoDto {
    private String description;
    private FetchExternalUrlDto external_urls;
    private String id;
    private SpotifyItemImageDto[] images;
    private String name;
    @JsonProperty("public")
    private boolean isPublic;
    private FetchUserPlaylistOwnerDto owner;
}
