package com.bonfire.BonfireArtistService.dtos.fetchuserplaylistsdtos;

import com.bonfire.BonfireArtistService.dtos.FetchExternalUrlDto;
import lombok.Data;

@Data
public class FetchUserPlaylistOwnerDto {
    private String display_name;
    private FetchExternalUrlDto external_urls;
}
