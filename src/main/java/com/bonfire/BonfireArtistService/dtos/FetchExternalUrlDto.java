package com.bonfire.BonfireArtistService.dtos;

import lombok.Data;

@Data
public class FetchExternalUrlDto {
    private String spotify;
}
