package com.bonfire.BonfireArtistService.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class SearchPlaylistDto {
    private String user;
    private String playlistName;
}
