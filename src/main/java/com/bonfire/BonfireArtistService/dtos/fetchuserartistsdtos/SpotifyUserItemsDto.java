package com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos;

import com.bonfire.BonfireArtistService.dtos.FetchExternalUrlDto;
import com.bonfire.BonfireArtistService.dtos.SpotifyItemImageDto;
import lombok.Data;

@Data
public class SpotifyUserItemsDto {
    private FetchExternalUrlDto external_urls;
    private String name;
    private String id;
    private SpotifyItemImageDto[] images;
    private String[] genres;
}
