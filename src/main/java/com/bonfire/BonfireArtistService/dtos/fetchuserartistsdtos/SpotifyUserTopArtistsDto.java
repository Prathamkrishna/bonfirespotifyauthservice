package com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos;

import lombok.Data;

@Data
public class SpotifyUserTopArtistsDto {
    private SpotifyUserItemsDto[] items;
}

