package com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class FetchArtistDto {
    private String artist;
    private String artistId;
    private String artistLink;
    private String artistImage;
}
