//package com.example.bonfirespotifyauthservice.controllers;
//
//import com.example.bonfirespotifyauthservice.security.CurrentUserDetailsService;
//import JwtUtil;
//import org.springframework.security.authentication.AnonymousAuthenticationToken;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@RestController("/api/v1/jwt")
//public class JWTController {
//
//    private final AuthenticationManager authenticationManager;
//    private final CurrentUserDetailsService currentUserDetailsService;
//    private final JwtUtil jwtUtil;
//
//    public JWTController(AuthenticationManager authenticationManager, CurrentUserDetailsService currentUserDetailsService, JwtUtil jwtUtil) {
//        this.authenticationManager = authenticationManager;
//        this.currentUserDetailsService = currentUserDetailsService;
//        this.jwtUtil = jwtUtil;
//    }
//
//    @PostMapping
//    public Map<String, String> getJwtToken(@RequestBody String spotifyRefreshToken){
//        try {
//            authenticationManager
//                    .authenticate(new UsernamePasswordAuthenticationToken(spotifyRefreshToken, null));
//        } catch (BadCredentialsException e){
//            e.printStackTrace();
//        }
////        see this
//        final UserDetails userDetails = currentUserDetailsService
//                .loadUserByUsername(spotifyRefreshToken);
//        final String jwt = jwtUtil.generateToken(userDetails.getUsername());
//        return new HashMap<>(){
//            {
//                put("JWT", jwt);
//            }
//        };
//    }
//}
