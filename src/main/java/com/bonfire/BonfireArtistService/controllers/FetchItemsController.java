package com.bonfire.BonfireArtistService.controllers;

import com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos.FetchArtistDto;
import com.bonfire.BonfireArtistService.services.interfaces.FetchUserArtistsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.bonfire.BonfireArtistService.utils.CacheMap.fetchArtistData;

@RestController
@RequestMapping(value = "/fetch")
public class FetchItemsController {

    private final FetchUserArtistsService fetchUserArtistsService;

    public FetchItemsController(FetchUserArtistsService fetchUserArtistsService) {
        this.fetchUserArtistsService = fetchUserArtistsService;
    }

    @GetMapping("/artist/{artist_name}")
    public ResponseEntity<FetchArtistDto> getArtist(@PathVariable("artist_name") String artist_name){
        FetchArtistDto fetchArtistDto = fetchArtistData(artist_name);
        if (fetchArtistDto == null){
            fetchArtistDto = fetchUserArtistsService.getArtist(artist_name);
        }
        return ResponseEntity.status(HttpStatus.OK)
                .body(fetchArtistDto);
    }
}
