package com.bonfire.BonfireArtistService.controllers;

import com.bonfire.BonfireArtistService.models.RegisterNewUserModel;
import com.bonfire.BonfireArtistService.dtos.fetchuseremailandprofiledtos.SpotifyUserProfileItemsDto;
import com.bonfire.BonfireArtistService.services.interfaces.LoadUserEmailAndProfile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fetchuserdata")
public class UserDataController {

    private final LoadUserEmailAndProfile loadUserEmailAndProfile;

    public UserDataController(LoadUserEmailAndProfile loadUserEmailAndProfile) {
        this.loadUserEmailAndProfile = loadUserEmailAndProfile;
    }

    @PostMapping("/")
    public ResponseEntity<SpotifyUserProfileItemsDto> loadUserEmail(@RequestBody RegisterNewUserModel registerNewUserModel){
        return loadUserEmailAndProfile.getUserEmail(registerNewUserModel);
    }
}
