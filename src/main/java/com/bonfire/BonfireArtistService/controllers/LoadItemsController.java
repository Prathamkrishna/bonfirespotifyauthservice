package com.bonfire.BonfireArtistService.controllers;

import com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos.SpotifyUserTopArtistsDto;
import com.bonfire.BonfireArtistService.dtos.fetchuserplaylistsdtos.FetchUserPlaylistInfoDto;
import com.bonfire.BonfireArtistService.models.SpotifyTopItemsRequestModel;
import com.bonfire.BonfireArtistService.services.interfaces.FetchUserArtistsService;
import com.bonfire.BonfireArtistService.services.interfaces.LoadUserPlaylistsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/load")
public class LoadItemsController {

    private final FetchUserArtistsService fetchUserArtistsService;
    private final LoadUserPlaylistsService loadUserPlaylistsService;

    public LoadItemsController(FetchUserArtistsService fetchUserArtistsService, LoadUserPlaylistsService loadUserPlaylistsService) {
        this.fetchUserArtistsService = fetchUserArtistsService;
        this.loadUserPlaylistsService = loadUserPlaylistsService;
    }

    @PostMapping("/usertopartists")
    public ResponseEntity<SpotifyUserTopArtistsDto> getTopSpotifyArtistsForUser(@RequestBody SpotifyTopItemsRequestModel spotifyTopItemsRequestModel){
        return fetchUserArtistsService.getTopArtists(spotifyTopItemsRequestModel.getAuthAPItoken(), spotifyTopItemsRequestModel.getTop(), spotifyTopItemsRequestModel.getName());
    }


    @PostMapping("/userplaylists")
    public ResponseEntity<List<FetchUserPlaylistInfoDto>> loadUserPlaylists(@RequestBody SpotifyTopItemsRequestModel topItemsRequestModel){
        return loadUserPlaylistsService
                .fetchPlaylists(topItemsRequestModel);
    }
}
