package com.bonfire.BonfireArtistService.repositories.interfaces;

import com.bonfire.BonfireArtistService.dtos.fetchuseremailandprofiledtos.SpotifyUserProfileItemsDto;

public interface AddUserEmailAndProfileRepository {
    void addUserEmailProfileDetails(SpotifyUserProfileItemsDto userProfileItemsDto);
    boolean checkUserEmailProfileDetailsExist(String mail);
}
