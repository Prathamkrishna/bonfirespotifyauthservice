package com.bonfire.BonfireArtistService.repositories.interfaces;

import com.bonfire.BonfireArtistService.dtos.ArtistGenreDto;
import com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos.SpotifyUserItemsDto;

import java.util.List;
import java.util.Map;

public interface AddUserArtistsAndGenreRepository {
    void addUserTopArtists(SpotifyUserItemsDto spotifyUserItemsDto, String name);
    void addUserTopGenres(Map<String, Integer> topGenres, String name);
    void addArtistAndGenres(List<ArtistGenreDto> artistGenreDto);
}
