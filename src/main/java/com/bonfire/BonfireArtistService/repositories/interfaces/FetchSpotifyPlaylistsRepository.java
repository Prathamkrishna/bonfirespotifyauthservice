package com.bonfire.BonfireArtistService.repositories.interfaces;

import com.bonfire.BonfireArtistService.dtos.GetUserPlaylistsDto;
import com.bonfire.BonfireArtistService.dtos.fetchuserplaylistsdtos.FetchUserPlaylistInfoDto;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface FetchSpotifyPlaylistsRepository extends ReactiveCrudRepository<FetchUserPlaylistInfoDto, Long> {
    @Query("select * from spotifyplaylists;")
    Flux<GetUserPlaylistsDto> getPlaylists();
}
