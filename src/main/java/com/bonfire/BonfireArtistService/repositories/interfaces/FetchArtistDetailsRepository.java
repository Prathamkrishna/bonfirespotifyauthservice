package com.bonfire.BonfireArtistService.repositories.interfaces;

import com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos.FetchArtistDto;

public interface FetchArtistDetailsRepository {
    FetchArtistDto fetchArtistDetails(String artistName);
}
