package com.bonfire.BonfireArtistService.repositories.interfaces;

import com.bonfire.BonfireArtistService.dtos.SearchPlaylistDto;
import com.bonfire.BonfireArtistService.dtos.fetchuserplaylistsdtos.FetchUserPlaylistInfoDto;

import java.util.List;

public interface SpotifyPlaylistsRepository {
    void addUserPlaylists(List<FetchUserPlaylistInfoDto> userPlaylistInfoDtos);
    Boolean searchUserPlaylists(SearchPlaylistDto searchPlaylistDto);
}
