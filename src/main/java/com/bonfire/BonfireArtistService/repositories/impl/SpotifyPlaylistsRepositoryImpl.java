package com.bonfire.BonfireArtistService.repositories.impl;

import com.bonfire.BonfireArtistService.repositories.interfaces.SpotifyPlaylistsRepository;
import com.bonfire.BonfireArtistService.dtos.SearchPlaylistDto;
import com.bonfire.BonfireArtistService.dtos.fetchuserplaylistsdtos.FetchUserPlaylistInfoDto;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class SpotifyPlaylistsRepositoryImpl implements SpotifyPlaylistsRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public SpotifyPlaylistsRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

//    push to background thread
    @Override
    public void addUserPlaylists(List<FetchUserPlaylistInfoDto> userPlaylistInfoDtos) {
        String addPlaylistSql = "INSERT into spotifyplaylists (description, playlistlink, playlistid, playlistname, imageurl, username, userprofilelink) values (:desc, :playlistlink, :playlistid, :playlistname, :imageUrl, :display_name, :profilelink);";
        Map<String, String> addPlaylistParams = new HashMap<>();
        userPlaylistInfoDtos
                .forEach(userPlayListInfo -> {
                    if (!searchUserPlaylists(new SearchPlaylistDto(userPlayListInfo.getOwner().getDisplay_name(), userPlayListInfo.getName()))){
                        addPlaylistParams.put("desc", userPlayListInfo.getDescription());
                        addPlaylistParams.put("playlistlink", userPlayListInfo.getExternal_urls().getSpotify());
                        addPlaylistParams.put("playlistid", userPlayListInfo.getId());
                        addPlaylistParams.put("playlistname", userPlayListInfo.getName());
                        addPlaylistParams.put("imageUrl", userPlayListInfo.getImages()[0].getUrl());
                        addPlaylistParams.put("display_name", userPlayListInfo.getOwner().getDisplay_name());
                        addPlaylistParams.put("profilelink", userPlayListInfo.getOwner().getExternal_urls().getSpotify());
                        try {
                            namedParameterJdbcTemplate
                                    .update(addPlaylistSql, addPlaylistParams);
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
    }

    @Override
    public Boolean searchUserPlaylists(SearchPlaylistDto searchPlaylistDto) {
        String searchPlaylistSql = "Select count(*) from spotifyplaylists where playlistname = '" + searchPlaylistDto.getPlaylistName() +
                (searchPlaylistDto.getUser() == null ? "';" : ("' and username = '" + searchPlaylistDto.getUser() + "'"));
        Integer count = -1;
        try {
            System.out.println("hi inside");
            count = namedParameterJdbcTemplate
                    .getJdbcTemplate()
                    .queryForObject(searchPlaylistSql, Integer.class);
        } catch (Exception e){
            e.printStackTrace();
        }
        assert count != null;
        return count != 0;
    }
}
