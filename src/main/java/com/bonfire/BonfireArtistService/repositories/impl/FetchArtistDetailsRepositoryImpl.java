package com.bonfire.BonfireArtistService.repositories.impl;

import com.bonfire.BonfireArtistService.repositories.interfaces.FetchArtistDetailsRepository;
import com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos.FetchArtistDto;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class FetchArtistDetailsRepositoryImpl implements FetchArtistDetailsRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public FetchArtistDetailsRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public FetchArtistDto fetchArtistDetails(String artistName) {
        List<FetchArtistDto> fetchArtistDto = new ArrayList<>();
        final String searchForArtist = "SELECT * FROM USERARTISTS WHERE artist = :artist ;";
        final Map<String, String> searchForArtistMap = new HashMap<>(){
            {put("artist", artistName);}
        };
        try {
            fetchArtistDto = namedParameterJdbcTemplate
                    .query(searchForArtist, searchForArtistMap, this::extractArtist);
        } catch (Exception e){
            e.printStackTrace();
        }
        System.out.println(fetchArtistDto);
        return fetchArtistDto.get(0);
    }

    private FetchArtistDto extractArtist(ResultSet rs, int rowCount){
        FetchArtistDto fetchArtistDto = new FetchArtistDto();
        try {
             fetchArtistDto.setArtist(rs.getString("artist"));
             fetchArtistDto.setArtistId(rs.getString("artistid"));
             fetchArtistDto.setArtistLink(rs.getString("artistlink"));
             fetchArtistDto.setArtistImage(rs.getString("artistimage"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fetchArtistDto;
    }
}
