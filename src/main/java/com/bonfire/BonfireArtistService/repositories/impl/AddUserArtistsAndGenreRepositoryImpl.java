package com.bonfire.BonfireArtistService.repositories.impl;

import com.bonfire.BonfireArtistService.dtos.ArtistGenreDto;
import com.bonfire.BonfireArtistService.repositories.interfaces.AddUserArtistsAndGenreRepository;
import com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos.SpotifyUserItemsDto;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AddUserArtistsAndGenreRepositoryImpl implements AddUserArtistsAndGenreRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public AddUserArtistsAndGenreRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public void addUserTopArtists(SpotifyUserItemsDto spotifyUserItemsDto, String name) {
        final String addTopArtistsSql = "INSERT INTO USERARTISTS (username, artist, artistid, artistlink, artistimage) values (:name, :artistName, :artistId, :artistLink, :artistImageLink);";
        Map<String, String> addTopArtistsParams = new HashMap<>();
        addTopArtistsParams.put("name", name);
        addTopArtistsParams.put("artistName", spotifyUserItemsDto.getName());
        addTopArtistsParams.put("artistId", spotifyUserItemsDto.getId());
        addTopArtistsParams.put("artistLink", spotifyUserItemsDto.getExternal_urls().getSpotify());
        addTopArtistsParams.put("artistImageLink", spotifyUserItemsDto.getImages()[0].getUrl());
        namedParameterJdbcTemplate
                .update(addTopArtistsSql, addTopArtistsParams);
    }

    @Override
    public void addUserTopGenres(Map<String, Integer> topGenres, String name) {
        String addTopGenresSql = "INSERT INTO USERGENRES (username, genre) values ";
        Object[] topGenresKeyArray = topGenres.keySet().toArray();
        for (int i = 0; i < topGenresKeyArray.length; i++) {
            if (i == topGenresKeyArray.length-1){
                addTopGenresSql = addTopGenresSql + "('" + name + "', '" + topGenresKeyArray[i] + "');";
            } else {
                addTopGenresSql = addTopGenresSql + "('" + name + "', '" + topGenresKeyArray[i] + "'),";
            }
        }
        namedParameterJdbcTemplate
                .getJdbcTemplate()
                .execute(addTopGenresSql);
    }

    @Override
    public void addArtistAndGenres(List<ArtistGenreDto> artistGenreDtos) {
        String addArtistsAndGenreSql = "INSERT IGNORE INTO ARTISTGENRES (artist, genre) values ";
        for(ArtistGenreDto artistGenreDto: artistGenreDtos) {
            for (int i = 0; i < artistGenreDto.getGenres().length; i++) {
                if (artistGenreDto == artistGenreDtos.get(artistGenreDtos.size() - 1) && i == artistGenreDto.getGenres().length-1) {
                    addArtistsAndGenreSql = addArtistsAndGenreSql + "('" + artistGenreDto.getArtistName() + "', '" + artistGenreDto.getGenres()[i] + "');";
                } else {
                    addArtistsAndGenreSql = addArtistsAndGenreSql + "('" + artistGenreDto.getArtistName() + "', '" + artistGenreDto.getGenres()[i] + "'),";
                }
            }
        }
        namedParameterJdbcTemplate
                .getJdbcTemplate()
                .execute(addArtistsAndGenreSql);
    }
}
