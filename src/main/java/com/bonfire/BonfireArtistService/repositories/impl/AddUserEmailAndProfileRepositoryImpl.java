package com.bonfire.BonfireArtistService.repositories.impl;

import com.bonfire.BonfireArtistService.repositories.interfaces.AddUserEmailAndProfileRepository;
import com.bonfire.BonfireArtistService.dtos.fetchuseremailandprofiledtos.SpotifyUserProfileItemsDto;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class AddUserEmailAndProfileRepositoryImpl implements AddUserEmailAndProfileRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public AddUserEmailAndProfileRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public void addUserEmailProfileDetails(SpotifyUserProfileItemsDto userProfileItemsDto) {
        String addUserQuery = "Insert into userinfo (useremail, userprofilelink, display_name) values (:email, :profile, :name);";
        Map<String, String> addUserParams = new HashMap<>();
        addUserParams.put("email", userProfileItemsDto.getEmail());
        addUserParams.put("profile", userProfileItemsDto.getExternal_urls().getSpotify());
        addUserParams.put("name", userProfileItemsDto.getDisplay_name());
        try{
            namedParameterJdbcTemplate
                    .update(addUserQuery, addUserParams);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean checkUserEmailProfileDetailsExist(String mail) {
        Integer numberOfRecords = -1;
        String searchUserQuery = "Select count(*) from userinfo where useremail = :email;";
        Map<String, String> searchUserParams = new HashMap<>();
        searchUserParams.put("email", mail);
        try {
            numberOfRecords = namedParameterJdbcTemplate
                    .queryForObject(searchUserQuery, searchUserParams, Integer.class);
        } catch (Exception e){
            e.printStackTrace();
        }
        assert numberOfRecords != null;
        return numberOfRecords != 0;
    }
}
