package com.bonfire.BonfireArtistService.repositories.impl;

import com.bonfire.BonfireArtistService.dtos.GetUserPlaylistsDto;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

@Repository
public class TempClass {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public TempClass(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public List<GetUserPlaylistsDto> getPlaylists(){
        return namedParameterJdbcTemplate
                .query("Select * from spotifyplaylists", new HashMap<>(), this::dtomapper);
    }

    private GetUserPlaylistsDto dtomapper(ResultSet rs, int rowNum) throws SQLException {
        GetUserPlaylistsDto g = new GetUserPlaylistsDto();
        g.setDescription(rs.getString("description"));
        g.setUsername(rs.getString("username"));
        g.setImageurl(rs.getString("imageurl"));
        g.setPlaylistid(rs.getString("playlistid"));
        g.setPlaylistlink(rs.getString("playlistlink"));
        g.setPlaylistname(rs.getString("playlistname"));
        g.setUserprofilelink(rs.getString("userprofilelink"));
        return g;
    }
}
