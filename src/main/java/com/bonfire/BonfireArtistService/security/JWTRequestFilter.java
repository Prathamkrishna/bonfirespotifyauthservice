//package com.example.bonfirespotifyauthservice.security;
//
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
//import org.springframework.stereotype.Component;
//import org.springframework.web.filter.OncePerRequestFilter;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
//@Component
//public class JWTRequestFilter extends OncePerRequestFilter {
//
//    private final CurrentUserDetailsService currentUserDetailsService;
//    private final JwtUtil jwtUtil;
//
//    public JWTRequestFilter(CurrentUserDetailsService currentUserDetailsService, JwtUtil jwtUtil) {
//        this.currentUserDetailsService = currentUserDetailsService;
//        this.jwtUtil = jwtUtil;
//    }
//
//    @Override
//    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
//        final String jwtHeader = request.getHeader("Authorization");
//        String spotifyRefreshToken = null;
//        String jwt = null;
//        if (jwtHeader != null && jwtHeader.startsWith("Bearer ")){
//            jwt = jwtHeader.substring(7);
//            spotifyRefreshToken = jwtUtil.extractRefreshToken(jwt);
//        }
//        if (spotifyRefreshToken != null && SecurityContextHolder.getContext().getAuthentication() == null){
//            UserDetails userDetails = currentUserDetailsService.loadUserByUsername(spotifyRefreshToken);
//            if (jwtUtil.validateToken(jwt, userDetails.getUsername())){
//                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
//                        new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
//                usernamePasswordAuthenticationToken
//                        .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
//            }
//        }
//        filterChain.doFilter(request, response);
//    }
//}
