package com.bonfire.BonfireArtistService.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtUtil {
//    change this secret key
    private final String SECRET_KEY = "secret_key";

    public String extractRefreshToken(String jwtToken){
        return extractClaims(jwtToken, Claims::getSubject);
    }

    public Date extractRefreshTokenExpiration(String jwtToken){
        return extractClaims(jwtToken, Claims::getExpiration);
    }

    public <T> T extractClaims(String jwtToken, Function<Claims, T> claimsTFunction){
        final Claims claims = extractAllClaims(jwtToken);
        return claimsTFunction.apply(claims);
    }

    private Claims extractAllClaims(String jwtToken){
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(jwtToken).getBody();
    }

    private Boolean isCurrentTokenExpired(String token){
        return extractRefreshTokenExpiration(token).before(new Date());
    }

    public String generateToken(String refreshToken){
        Map<String, Object> claims = new HashMap<>();
        return createNewJwtToken(claims, refreshToken);
    }

    private String createNewJwtToken(Map<String, Object> claims, String subjectRefreshToken){
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subjectRefreshToken)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000*60*60*24*7))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                .compact();
    }

    public Boolean validateToken(String token, String refreshToken){
        final String extractedRefreshToken = extractRefreshToken(token);
        return (extractedRefreshToken.equals(refreshToken) && !isCurrentTokenExpired(token));
    }
}
