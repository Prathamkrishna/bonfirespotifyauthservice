package com.bonfire.BonfireArtistService.models;

import lombok.Data;

@Data
public class SpotifyTopItemsRequestModel {
    private final String name;
    private final String authAPItoken;
    private final int top;
}
