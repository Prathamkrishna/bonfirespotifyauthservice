package com.bonfire.BonfireArtistService.models;

import lombok.Data;

@Data
public class RegisterNewUserModel {
    private String authAPItoken;
}
