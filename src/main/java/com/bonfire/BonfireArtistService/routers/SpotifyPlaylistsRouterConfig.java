package com.bonfire.BonfireArtistService.routers;

import com.bonfire.BonfireArtistService.services.interfaces.FetchUserPlaylistsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class SpotifyPlaylistsRouterConfig {

    private final FetchUserPlaylistsService fetchUserPlaylistsService;

    public SpotifyPlaylistsRouterConfig(FetchUserPlaylistsService fetchUserPlaylistsService) {
        this.fetchUserPlaylistsService = fetchUserPlaylistsService;
    }

    @Bean
    public RouterFunction<ServerResponse> routerFunction(){
        return RouterFunctions.route()
                .GET("/fetchuserplaylists", fetchUserPlaylistsService::getPlaylists)
                .build();
    }
}
