package com.bonfire.BonfireArtistService.utils;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

public class HttpHeadersUtil {
    public static HttpEntity<String> setHeaders(String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + token);
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json");
        return new HttpEntity<>(headers);
    }
}
