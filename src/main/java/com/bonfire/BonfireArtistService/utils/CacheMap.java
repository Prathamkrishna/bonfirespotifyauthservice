package com.bonfire.BonfireArtistService.utils;

import com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos.FetchArtistDto;

import java.util.HashMap;
import java.util.Map;

public class CacheMap {
    private static final Map<String, FetchArtistDto> artistDataMap = new HashMap<>();
    private static final Map<String, Integer> artistDataFetchCountMap = new HashMap<>();

    public static FetchArtistDto fetchArtistData(String artist){
        FetchArtistDto artistData = artistDataMap.get(artist);
        if (artistData != null) increaseArtistDataCount(artist);
        return artistData;
    }

    protected static void increaseArtistDataCount(String artist){
        artistDataFetchCountMap.put(artist, artistDataFetchCountMap.get(artist)+1);
    }

    public static void addArtistData(String artist, FetchArtistDto data){
        artistDataMap.put(artist, data);
        addArtistCount(artist);
    }

    protected static void addArtistCount(String artist){
        artistDataFetchCountMap.put(artist, 1);
    }
}
