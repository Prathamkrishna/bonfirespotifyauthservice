package com.bonfire.BonfireArtistService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BonfireArtistService {

    public static void main(String[] args) {
        SpringApplication.run(BonfireArtistService.class, args);
    }

}
