package com.bonfire.BonfireArtistService.services.impl;

import com.bonfire.BonfireArtistService.repositories.interfaces.FetchArtistDetailsRepository;
import com.bonfire.BonfireArtistService.utils.CacheMap;
import com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos.FetchArtistDto;
import com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos.SpotifyUserItemsDto;
import com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos.SpotifyUserTopArtistsDto;
import com.bonfire.BonfireArtistService.services.interfaces.ExtractArtistGenreService;
import com.bonfire.BonfireArtistService.services.interfaces.FetchUserArtistsService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import static com.bonfire.BonfireArtistService.utils.HttpHeadersUtil.setHeaders;

@Service
public class FetchUserArtistsServiceImpl implements FetchUserArtistsService {

    private final String spotifyGetUserTopArtists = "https://api.spotify.com/v1/me/top/artists?time_range=medium_term";

    private final RestTemplate restTemplate;
    private final ExtractArtistGenreService extractArtistAndGenreService;
    private final FetchArtistDetailsRepository fetchArtistDetailsRepository;

    public FetchUserArtistsServiceImpl(RestTemplate restTemplate, ExtractArtistGenreService extractArtistAndGenreService, FetchArtistDetailsRepository fetchArtistDetailsRepository) {
        this.restTemplate = restTemplate;
        this.extractArtistAndGenreService = extractArtistAndGenreService;
        this.fetchArtistDetailsRepository = fetchArtistDetailsRepository;
    }

    @Override
    public ResponseEntity<SpotifyUserTopArtistsDto> getTopArtists(String authToken, int usercount, String name) {
        String userUrl = spotifyGetUserTopArtists + "&limit=" + usercount;
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        HttpEntity<String> httpEntity = setHeaders(authToken);
        ResponseEntity<String> responseEntity = null;
        SpotifyUserTopArtistsDto topArtistsDto = new SpotifyUserTopArtistsDto();
        try {
            responseEntity = restTemplate
                .exchange(userUrl, HttpMethod.GET, httpEntity, String.class);
            if (responseEntity.getStatusCodeValue() != 401) {
                topArtistsDto = objectMapper.readValue(responseEntity.getBody(), SpotifyUserTopArtistsDto.class);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        if (responseEntity != null) {
            System.out.println("ok");
            //test and if required use Completable future
            extractArtistAndGenreService.setSpotifyUserItemsDto(topArtistsDto);
            extractArtistAndGenreService.setName(name);
            extractArtistAndGenreService.filterArtists();
            extractArtistAndGenreService.filterGenres();
            extractArtistAndGenreService.filterArtistsAndGenre();
        }
        return ResponseEntity
                .status(200)
                .body(topArtistsDto);
    }

    @Override
    public FetchArtistDto getArtist(String artist) {
        SpotifyUserItemsDto spotifyUserItemsDto = null;
//        repository
        FetchArtistDto fetchArtistDto = fetchArtistDetailsRepository.fetchArtistDetails(artist);
        CacheMap.addArtistData(fetchArtistDto.getArtist(), fetchArtistDto);
        return fetchArtistDto;
    }
}
