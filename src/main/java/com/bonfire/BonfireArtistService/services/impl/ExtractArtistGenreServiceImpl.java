package com.bonfire.BonfireArtistService.services.impl;

import com.bonfire.BonfireArtistService.dtos.ArtistGenreDto;
import com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos.SpotifyUserItemsDto;
import com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos.SpotifyUserTopArtistsDto;
import com.bonfire.BonfireArtistService.repositories.interfaces.AddUserArtistsAndGenreRepository;
import com.bonfire.BonfireArtistService.services.interfaces.ExtractArtistGenreService;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ExtractArtistGenreServiceImpl implements ExtractArtistGenreService {

    private final AddUserArtistsAndGenreRepository addUserArtistsAndGenreRepository;
    private SpotifyUserTopArtistsDto spotifyUserTopArtistsDto;
    private String name;

    public void setSpotifyUserItemsDto(SpotifyUserTopArtistsDto spotifyUserTopArtistsDto) {
        this.spotifyUserTopArtistsDto = spotifyUserTopArtistsDto;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ExtractArtistGenreServiceImpl(AddUserArtistsAndGenreRepository addUserArtistsAndGenreRepository) {
        this.addUserArtistsAndGenreRepository = addUserArtistsAndGenreRepository;
    }

    @Override
    public void filterGenres() {
        Map<String, Integer> topGenreCount = new HashMap<>();
        int maxCount = 0;
        for (SpotifyUserItemsDto spotifyUserItemsDto : spotifyUserTopArtistsDto.getItems()){
            for (int i = 0; i < spotifyUserItemsDto.getGenres().length; i++) {
                String genre = spotifyUserItemsDto.getGenres()[i];
                topGenreCount.put(genre, topGenreCount.getOrDefault(genre, 0)+1);
                if (topGenreCount.get(genre) > maxCount) maxCount = topGenreCount.get(genre);
            }
        }
        topGenreCount = sortOrder(topGenreCount);
        addUserArtistsAndGenreRepository.addUserTopGenres(topGenreCount, name);
    }

    @Override
    public void filterArtists() {
        for (SpotifyUserItemsDto spotifyUserItemsDto: spotifyUserTopArtistsDto.getItems()){
            addUserArtistsAndGenreRepository.addUserTopArtists(spotifyUserItemsDto, name);
        }
    }


    @Override
    public void filterArtistsAndGenre() {
        List<ArtistGenreDto> artistGenreDto = new ArrayList<>();
        for (int i = 0; i < spotifyUserTopArtistsDto.getItems().length; i++) {
            artistGenreDto.add(new ArtistGenreDto(spotifyUserTopArtistsDto.getItems()[i].getName(), spotifyUserTopArtistsDto.getItems()[i].getGenres()));
        }
        addUserArtistsAndGenreRepository.addArtistAndGenres(artistGenreDto);
    }

    private Map<String, Integer> sortOrder(Map<String, Integer> genreMap){
        return genreMap.entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .limit(5)
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }
}
