package com.bonfire.BonfireArtistService.services.impl;

import com.bonfire.BonfireArtistService.dtos.fetchuseremailandprofiledtos.SpotifyUserProfileItemsDto;
import com.bonfire.BonfireArtistService.models.RegisterNewUserModel;
import com.bonfire.BonfireArtistService.repositories.interfaces.AddUserEmailAndProfileRepository;
import com.bonfire.BonfireArtistService.services.interfaces.LoadUserEmailAndProfile;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class LoadUserEmailAndProfileImpl implements LoadUserEmailAndProfile {

    private final String spotifyFetchUserEmailAndProfileUrl = "https://api.spotify.com/v1/me/";

    private final RestTemplate restTemplate;
    private final AddUserEmailAndProfileRepository addUserEmailAndProfileRepository;

    public LoadUserEmailAndProfileImpl(RestTemplate restTemplate, AddUserEmailAndProfileRepository addUserEmailAndProfileRepository) {
        this.restTemplate = restTemplate;
        this.addUserEmailAndProfileRepository = addUserEmailAndProfileRepository;
    }

    @Override
    public ResponseEntity<SpotifyUserProfileItemsDto> getUserEmail(RegisterNewUserModel registerNewUserModel) {
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + registerNewUserModel.getAuthAPItoken());
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json");
        HttpEntity<String> httpEntity = new HttpEntity<>(headers);
        SpotifyUserProfileItemsDto profileItemsDto = new SpotifyUserProfileItemsDto();
        try {
            ResponseEntity<String> responseEntity = restTemplate
                    .exchange(spotifyFetchUserEmailAndProfileUrl, HttpMethod.GET, httpEntity, String.class);
             profileItemsDto = objectMapper.readValue(responseEntity.getBody(), SpotifyUserProfileItemsDto.class);
        } catch (Exception e){
            e.printStackTrace();
        }
        if (profileItemsDto.getEmail() != null) {
            if (!addUserEmailAndProfileRepository.checkUserEmailProfileDetailsExist(profileItemsDto.getEmail())) {
                addUserEmailAndProfileRepository
                        .addUserEmailProfileDetails(profileItemsDto);
                profileItemsDto.setUserExists(false);
            } else profileItemsDto.setUserExists(true);
        }
        return ResponseEntity
                .status(200)
                .body(profileItemsDto);
    }
}
