package com.bonfire.BonfireArtistService.services.impl;

import com.bonfire.BonfireArtistService.repositories.interfaces.FetchSpotifyPlaylistsRepository;
import com.bonfire.BonfireArtistService.dtos.GetUserPlaylistsDto;
import com.bonfire.BonfireArtistService.services.interfaces.FetchUserPlaylistsService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@Service
public class FetchUserPlaylistsServiceImpl implements FetchUserPlaylistsService {
    private final FetchSpotifyPlaylistsRepository fetchSpotifyPlaylistsRepository;

    public FetchUserPlaylistsServiceImpl(FetchSpotifyPlaylistsRepository fetchSpotifyPlaylistsRepository) {
        this.fetchSpotifyPlaylistsRepository = fetchSpotifyPlaylistsRepository;
    }

    @Override
    public Mono<ServerResponse> getPlaylists(ServerRequest request) {
        Flux<GetUserPlaylistsDto> fetchPlaylistsFlux = fetchSpotifyPlaylistsRepository.getPlaylists().delayElements(Duration.ofSeconds(1));
        return ServerResponse.ok()
                .contentType(MediaType.TEXT_EVENT_STREAM)
                .body(Mono.from(fetchPlaylistsFlux), GetUserPlaylistsDto.class);
    }
}
