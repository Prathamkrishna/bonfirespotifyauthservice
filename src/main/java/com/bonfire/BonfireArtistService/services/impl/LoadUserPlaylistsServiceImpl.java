package com.bonfire.BonfireArtistService.services.impl;

import com.bonfire.BonfireArtistService.models.SpotifyTopItemsRequestModel;
import com.bonfire.BonfireArtistService.utils.HttpHeadersUtil;
import com.bonfire.BonfireArtistService.dtos.fetchuserplaylistsdtos.FetchUserPlaylistInfoDto;
import com.bonfire.BonfireArtistService.dtos.fetchuserplaylistsdtos.FetchUserPlaylistItemsDto;
import com.bonfire.BonfireArtistService.repositories.interfaces.SpotifyPlaylistsRepository;
import com.bonfire.BonfireArtistService.services.interfaces.LoadUserPlaylistsService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

@Service
public class LoadUserPlaylistsServiceImpl implements LoadUserPlaylistsService {

    private final String fetchPlaylistsUrl = "https://api.spotify.com/v1/me/playlists?";

    private final RestTemplate restTemplate;
    private final SpotifyPlaylistsRepository spotifyPlaylistsRepository;
    private final Executor executor;

    public LoadUserPlaylistsServiceImpl(RestTemplate restTemplate, SpotifyPlaylistsRepository spotifyPlaylistsRepository, Executor executor) {
        this.restTemplate = restTemplate;
        this.spotifyPlaylistsRepository = spotifyPlaylistsRepository;
        this.executor = executor;
    }

    @Override
    public ResponseEntity<List<FetchUserPlaylistInfoDto>> fetchPlaylists(SpotifyTopItemsRequestModel topItemsRequestModel) {
        String finalUrl = fetchPlaylistsUrl+"limit="+topItemsRequestModel.getTop();
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        HttpEntity<String> httpEntity = HttpHeadersUtil.setHeaders(topItemsRequestModel.getAuthAPItoken());
        FetchUserPlaylistItemsDto itemsDto = new FetchUserPlaylistItemsDto();
        List<FetchUserPlaylistInfoDto> fetchUserPlaylistInfo = new ArrayList<>();
        try {
            ResponseEntity<String> responseEntity = restTemplate
                    .exchange(finalUrl, HttpMethod.GET, httpEntity, String.class);
            if (responseEntity.getBody() != null) {
                itemsDto = objectMapper.readValue(responseEntity.getBody(), FetchUserPlaylistItemsDto.class);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
//        get data from ppls spotify forms
//        use for loop for multiple requests
        if (itemsDto.getItems() != null) {
            fetchUserPlaylistInfo = Arrays.stream(itemsDto.getItems())
                    .filter(val -> !val.getOwner().getDisplay_name().equals("Spotify"))
                    .filter(FetchUserPlaylistInfoDto::isPublic)
                    .collect(Collectors.toList());
        }
        List<FetchUserPlaylistInfoDto> finalFetchUserPlaylistInfo = fetchUserPlaylistInfo;
        CompletableFuture
                .runAsync(()-> spotifyPlaylistsRepository.addUserPlaylists(finalFetchUserPlaylistInfo), executor);
        return ResponseEntity
                .status(200)
                .body(fetchUserPlaylistInfo);
    }
}
