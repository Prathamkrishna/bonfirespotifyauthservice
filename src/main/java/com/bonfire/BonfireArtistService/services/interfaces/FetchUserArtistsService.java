package com.bonfire.BonfireArtistService.services.interfaces;

import com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos.SpotifyUserTopArtistsDto;
import com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos.FetchArtistDto;
import org.springframework.http.ResponseEntity;

public interface FetchUserArtistsService {
    ResponseEntity<SpotifyUserTopArtistsDto> getTopArtists(String authToken, int usercount, String name);
    FetchArtistDto getArtist(String artist);
}
