package com.bonfire.BonfireArtistService.services.interfaces;

import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

public interface FetchUserPlaylistsService {
    Mono<ServerResponse> getPlaylists(ServerRequest request);
}
