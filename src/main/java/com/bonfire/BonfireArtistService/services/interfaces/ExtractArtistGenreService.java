package com.bonfire.BonfireArtistService.services.interfaces;

import com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos.SpotifyUserItemsDto;
import com.bonfire.BonfireArtistService.dtos.fetchuserartistsdtos.SpotifyUserTopArtistsDto;

public interface ExtractArtistGenreService {
    void filterGenres();
    void filterArtists();
    void filterArtistsAndGenre();
    void setSpotifyUserItemsDto(SpotifyUserTopArtistsDto spotifyUserItemsDto);
    void setName(String name);
}
