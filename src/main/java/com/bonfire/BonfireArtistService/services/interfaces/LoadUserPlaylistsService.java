package com.bonfire.BonfireArtistService.services.interfaces;

import com.bonfire.BonfireArtistService.models.SpotifyTopItemsRequestModel;
import com.bonfire.BonfireArtistService.dtos.fetchuserplaylistsdtos.FetchUserPlaylistInfoDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface LoadUserPlaylistsService {
    ResponseEntity<List<FetchUserPlaylistInfoDto>> fetchPlaylists(SpotifyTopItemsRequestModel topItemsRequestModel);
}
