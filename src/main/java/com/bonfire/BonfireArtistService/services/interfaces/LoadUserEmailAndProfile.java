package com.bonfire.BonfireArtistService.services.interfaces;

import com.bonfire.BonfireArtistService.dtos.fetchuseremailandprofiledtos.SpotifyUserProfileItemsDto;
import com.bonfire.BonfireArtistService.models.RegisterNewUserModel;
import org.springframework.http.ResponseEntity;

public interface LoadUserEmailAndProfile {
    ResponseEntity<SpotifyUserProfileItemsDto> getUserEmail(RegisterNewUserModel registerNewUserModel);
}
